// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBSx5c2pUITrfc9dYiWyALZYvXwk9DHCe4",
    authDomain: "oshop-1cd43.firebaseapp.com",
    databaseURL: "https://oshop-1cd43.firebaseio.com",
    projectId: "oshop-1cd43",
    storageBucket: "oshop-1cd43.appspot.com",
    messagingSenderId: "4366902177"
  }
};
